package services

import (
	"auth/internal/config"
	"auth/internal/mongodb/models"
	"fmt"
	"time"

	"auth/internal/token"

	"go.uber.org/zap"
)

var LoginOrPasswordInvalid = fmt.Errorf("login or password invalid")
var RefreshTokenExpired = fmt.Errorf("refresh token expired")

type AuthService struct {
	log         *zap.Logger
	cfg         *config.AppConfig
	userService *UserService
}

type AuthResult struct {
	Token        string
	RefreshToken string
	User         *models.User
	Err          error
}

func NewAuthService(cfg *config.AppConfig, log *zap.Logger, userService *UserService) *AuthService {
	return &AuthService{
		cfg:         cfg,
		log:         log,
		userService: userService,
	}
}

func (s *AuthService) Login(login, password string) *AuthResult {
	user, err := s.userService.GetByLogin(login)
	if err != nil {
		return &AuthResult{
			Err: err,
		}
	}

	userPassword, err := s.userService.GetPassword(user.GUID)
	if err != nil {
		return &AuthResult{
			Err: err,
		}
	}

	valid := s.userService.CheckPasswordHash(password, userPassword)
	if !valid {
		return &AuthResult{
			Err: LoginOrPasswordInvalid,
		}
	}

	t, rt, err := s.generateTokens(user)
	if err != nil {
		return &AuthResult{
			Err: err,
		}
	}

	user.LastLoginAt = time.Now()

	return &AuthResult{
		Token:        t,
		RefreshToken: rt,
		User:         user,
		Err:          nil,
	}
}

func (s *AuthService) Refresh(guid string, refreshToken string) *AuthResult {
	user, err := s.userService.GetByGuid(guid)
	if err != nil {
		return &AuthResult{
			Err: err,
		}
	}

	_, valid := token.VerifyToken(s.cfg.TokenSecret, refreshToken)
	if !valid {
		return &AuthResult{
			Err: RefreshTokenExpired,
		}
	}

	if user.RefreshToken != refreshToken {
		return &AuthResult{
			Err: RefreshTokenExpired,
		}
	}

	t, rt, err := s.generateTokens(user)
	if err != nil {
		return &AuthResult{
			Err: err,
		}
	}

	user.LastLoginAt = time.Now()

	return &AuthResult{
		Token:        t,
		RefreshToken: rt,
		User:         user,
		Err:          nil,
	}
}

func (s *AuthService) generateTokens(user *models.User) (string, string, error) {
	t, err := token.NewToken(s.cfg.TokenSecret, s.cfg.TokenExpirationTimeMinutes, &token.TokenUserInfo{
		ID:    user.GUID,
		Login: user.Login,
		Name:  user.Name,
	})
	if err != nil {
		return "", "", err
	}

	rt, err := token.NewToken(s.cfg.TokenSecret, s.cfg.RefreshTokenExpirationTimeMinutes, nil)
	if err != nil {
		return "", "", err
	}

	err = s.userService.UpdateRefreshTokenAndLastLogin(user.GUID, rt)
	if err != nil {
		return "", "", err
	}

	return t, rt, nil
}
