package services

import (
	"auth/internal/config"
	"auth/internal/mongodb"
	"auth/internal/mongodb/models"
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.uber.org/zap"
	"golang.org/x/crypto/bcrypt"
)

var UserAlreadyExistsError = fmt.Errorf("user already exists")

type NewUser struct {
	Login    string
	Password string
	Name     string
	LastName string
}

type UserService struct {
	log         *zap.Logger
	mongoClient *mongodb.MongoDB
	dbClient    *sqlx.DB
	collection  string
}

func NewUserService(logger *zap.Logger, mongoClient *mongodb.MongoDB, db *sqlx.DB) *UserService {
	return &UserService{
		log:         logger,
		mongoClient: mongoClient,
		dbClient:    db,
		collection:  "users",
	}
}

func (s *UserService) GetByGuid(guid string) (*models.User, error) {
	var user *models.User
	collection := s.mongoClient.GetCollection(s.collection)
	err := collection.FindOne(context.TODO(), bson.M{"guid": guid}).Decode(&user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *UserService) GetByLogin(login string) (*models.User, error) {
	var user *models.User
	collection := s.mongoClient.GetCollection(s.collection)
	err := collection.FindOne(context.TODO(), bson.M{"login": login}).Decode(&user)
	if err == mongo.ErrNoDocuments {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return user, nil
}

func (s *UserService) GetPassword(guid string) (string, error) {
	var password string

	query := "SELECT password FROM passwords WHERE user_id = $1 and expired_at > now() ORDER BY expired_at DESC LIMIT 1"

	err := s.dbClient.QueryRow(query, guid).Scan(&password)
	if err != nil {
		return "", err
	}
	return password, nil
}

func (s *UserService) Register(ctx context.Context, nur *NewUser) (*models.User, error) {
	var user *models.User
	collection := s.mongoClient.GetCollection(s.collection)
	existedUser, err := s.GetByLogin(nur.Login)
	s.log.Debug("User already exists", zap.String("login", nur.Login), zap.Error(err))
	if existedUser != nil {
		return nil, UserAlreadyExistsError
	}

	if err != nil {
		return nil, err
	}

	userGuid := uuid.New().String()
	newUser := &models.User{
		Login:     nur.Login,
		GUID:      userGuid,
		LoginType: models.LoginType{ID: 1, Name: "email"},
		Name:      nur.Name,
		LastName:  nur.LastName,
		CreatedAt: time.Now(),
	}
	insertResult, err := collection.InsertOne(ctx, newUser)
	if err != nil {
		return nil, err
	}

	sql := `INSERT INTO passwords (user_id, password, expired_at) VALUES ($1, $2, $3)`
	hashedPwd, err := s.HashPassword(nur.Password)
	if err != nil {
		s.log.Error("Error while hashing password", zap.Error(err))
		err2 := s.DeleteByID(insertResult.InsertedID.(primitive.ObjectID))
		if err2 != nil {
			return nil, err2
		}
		return nil, err
	}

	cfg := ctx.Value("cfg").(*config.AppConfig)
	_, err = s.dbClient.ExecContext(ctx, sql, userGuid, hashedPwd, time.Now().Add(time.Duration(cfg.PasswordLifeTime)*time.Hour))
	if err != nil {
		s.log.Error("Error while saving password", zap.Error(err))
		err2 := s.DeleteByID(insertResult.InsertedID.(primitive.ObjectID))
		if err2 != nil {
			return nil, err2
		}
		return nil, err
	}

	return user, nil
}

func (s *UserService) DeleteByID(id primitive.ObjectID) error {
	collection := s.mongoClient.GetCollection(s.collection)
	_, err := collection.DeleteOne(context.TODO(), bson.M{"_id": id})
	if err != nil {
		return err
	}
	return nil
}

func (s *UserService) HashPassword(password string) (string, error) {
	hashed, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	if err != nil {
		return "", err
	}
	return string(hashed), nil
}

func (s *UserService) CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func (s *UserService) UpdateRefreshToken(guid string, refreshToken string) error {
	collection := s.mongoClient.GetCollection(s.collection)
	_, err := collection.UpdateOne(context.TODO(), bson.M{"guid": guid}, bson.M{"$set": bson.M{"refresh_token": refreshToken}})
	if err != nil {
		return err
	}
	return nil
}

func (s *UserService) UpdateLastLoginAt(guid string) error {
	collection := s.mongoClient.GetCollection(s.collection)
	_, err := collection.UpdateOne(context.TODO(), bson.M{"guid": guid}, bson.M{"$set": bson.M{"last_login_at": time.Now()}})
	if err != nil {
		return err
	}
	return nil
}

func (s *UserService) UpdateRefreshTokenAndLastLogin(guid string, refreshToken string) error {
	collection := s.mongoClient.GetCollection(s.collection)
	_, err := collection.UpdateOne(context.TODO(), bson.M{"guid": guid}, bson.M{"$set": bson.M{
		"refresh_token": refreshToken,
		"last_login_at": time.Now(),
	}})
	if err != nil {
		return err
	}
	return nil
}
