package config

import (
	"fmt"

	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
)

type WebServerConfig struct {
	Port int
}

type DBConnectionConfig struct {
	Type     string
	Host     string
	Port     int
	Database string
	Username string
	Password string `json:"-"`
	Pool     struct {
		MaxIdleConns int `mapstructure:"max-idle-conns"`
		MaxOpenConns int `mapstructure:"max-open-conns"`
		IdleTimeout  int `mapstructure:"idle-timeout"`
	}
}

type MongoDBConnectionConfig struct {
	Host     string
	Port     int
	Database string
	Username string
	Password string `json:"-"`
}

type AppConfig struct {
	Name                              string
	PasswordLifeTime                  int    `mapstructure:"password-life-time"` // in hours
	PasswordMinLength                 int    `mapstructure:"password-min-length"`
	TokenExpirationTimeMinutes        int    `mapstructure:"token-expiration-time-minutes"`         // in minutes
	RefreshTokenExpirationTimeMinutes int    `mapstructure:"refresh-token-expiration-time-minutes"` // in minutes
	TokenSecret                       string `mapstructure:"token-secret"`
}

type LoggingConfiguration struct {
	Level      string
	Path       string
	MaxSize    int `mapstructure:"max-size"`
	MaxBackups int `mapstructure:"max-backups"`
	MaxAge     int `mapstructure:"max-age"`
}

type Config struct {
	Debug   bool                     `mapstructure:"debug"`
	App     *AppConfig               `mapstructure:"app"`
	Logging *LoggingConfiguration    `mapstructure:"logging"`
	Mongo   *MongoDBConnectionConfig `mapstructure:"mongodb"`
	Web     *WebServerConfig         `mapstructure:"web"`
	DB      *DBConnectionConfig      `mapstructure:"db"`
}

var C *Config = new(Config)

func InitConfiguration() *Config {
	initConfig()
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		initConfig()
	})

	return C
}

func initConfig() {
	loadDefault()
	loadFile()
	loadEnv()

	if viper.GetBool("debug") {
		viper.SetDefault("logging.level", "debug")
	}

	viper.Unmarshal(C)
}

func loadEnv() {
	viper.SetEnvPrefix("auth")
	viper.AutomaticEnv()
}

func loadDefault() {
	viper.SetDefault("debug", false)
	viper.SetDefault("app.name", "mm-course-auth")
	viper.SetDefault("app.password-life-time", 1)
	viper.SetDefault("app.password-min-length", 8)
	viper.SetDefault("app.token-expiration-time-minutes", 5)
	viper.SetDefault("app.refresh-token-expiration-time-minutes", 60)
	viper.SetDefault("app.token-secret", "secret")

	viper.SetDefault("logging.level", "info")
	viper.SetDefault("logging.path", "logs")
	viper.SetDefault("logging.max-size", 500)
	viper.SetDefault("logging.max-backups", 3)
	viper.SetDefault("logging.max-age", 30)

	viper.SetDefault("mongodb.host", "localhost")
	viper.SetDefault("mongodb.port", 27017)
	viper.SetDefault("mongodb.database", "mm-course-auth")
	viper.SetDefault("mongodb.username", "root")
	viper.SetDefault("mongodb.password", "root")

	viper.SetDefault("web.port", 8080)

	viper.SetDefault("db.type", "postgres")
	viper.SetDefault("db.host", "localhost")
	viper.SetDefault("db.port", 5432)
	viper.SetDefault("db.username", "postgres")
	viper.SetDefault("db.password", "postgres")
	viper.SetDefault("db.database", "mm-course-auth")
	viper.SetDefault("db.pool.max-idle-conns", 1)
	viper.SetDefault("db.pool.max-open-conns", 10)
	viper.SetDefault("db.pool.idle-timeout", 300)
}

func loadFile() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	viper.AddConfigPath(".")
	viper.AddConfigPath("./config/")
	viper.AddConfigPath(fmt.Sprintf("$HOME/.%s", viper.GetString("app.name")))
	viper.AddConfigPath(fmt.Sprintf("/etc/%s/", viper.GetString("app.name")))
	viper.AddConfigPath(fmt.Sprintf("/etc/%s/config/", viper.GetString("app.name")))

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(fmt.Sprintf("Error reading config file, %s. Use default only.", err))
	}
}
