package logging

import (
	"auth/internal/config"
	"os"
	"path/filepath"

	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func NewLogger(cfg *config.LoggingConfiguration, name string) *zap.Logger {
	logger := zap.New(configure(cfg, name))
	return logger
}

func configure(cfg *config.LoggingConfiguration, name string) zapcore.Core {
	fileWriter := zapcore.AddSync(&lumberjack.Logger{
		Filename:   filepath.Join(cfg.Path, name),
		MaxSize:    cfg.MaxSize,
		MaxBackups: cfg.MaxBackups,
		MaxAge:     cfg.MaxAge,
	})

	var priority zap.LevelEnablerFunc
	switch cfg.Level {
	case "debug":
		priority = zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
			return lvl >= zap.DebugLevel
		})
	case "warn":
		priority = zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
			return lvl >= zap.WarnLevel
		})
	default:
		priority = zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
			return lvl >= zap.InfoLevel
		})
	}

	consoleWriter := zapcore.Lock(os.Stdout)
	jsonEncoder := zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig())
	consoleEncoder := zapcore.NewConsoleEncoder(zap.NewDevelopmentEncoderConfig())

	return zapcore.NewTee(
		zapcore.NewCore(consoleEncoder, consoleWriter, priority),
		zapcore.NewCore(jsonEncoder, fileWriter, priority),
	)
}
