package controllers

import (
	"auth/internal/config"
	"auth/internal/mongodb/models"
	"auth/internal/services"
	"auth/internal/token"
	"context"
	"fmt"
	"strconv"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

var AuthTokenRequired = fmt.Errorf("auth token required")
var InvalidAuthToken = fmt.Errorf("invalid auth token")
var LoginRequiredError = fmt.Errorf("login required")
var PasswordRequiredError = fmt.Errorf("password required")
var PasswordNotEqualError = fmt.Errorf("password not equal")
var PasswordMinLengthError = fmt.Errorf("password is too short")

type RegisterRequest struct {
	Login           string `json:"login" validate:"required"`
	Password        string `json:"password" validate:"required"`
	ConfirmPassword string `json:"confirm_password" validate:"required,eqfield=Password"`
	Name            string `json:"name" validate:"required"`
	LastName        string `json:"last_name,omitempty"`
}

func (r RegisterRequest) Validate(passwordMinLength int) []*ValidationError {
	var errors []*ValidationError
	err := validate.Struct(r)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element ValidationError
			element.FailedField = err.StructNamespace()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}

	if passwordMinLength > 0 && len(r.Password) < passwordMinLength {
		var element ValidationError
		element.FailedField = "RegisterRequest.Password"
		element.Tag = "minlength=" + strconv.Itoa(passwordMinLength)
		element.Value = r.Password
		errors = append(errors, &element)
	}

	return errors
}

type Response struct {
	Ok     bool               `json:"ok"`
	User   *models.User       `json:"user,omitempty"`
	Cause  string             `json:"cause,omitempty"`
	Errors []*ValidationError `json:"errors,omitempty"`
}

type UserController struct {
	cfg         *config.AppConfig
	log         *zap.Logger
	userService *services.UserService
}

func NewUserController(cfg *config.AppConfig, log *zap.Logger, userService *services.UserService) *UserController {
	return &UserController{
		cfg:         cfg,
		log:         log,
		userService: userService,
	}
}

func (c *UserController) GetGroup() string {
	return "/auth"
}

func (c *UserController) GetHandlers() []ControllerHandler {
	return []ControllerHandler{
		&Handler{
			Method:  "POST",
			Path:    "/register",
			Handler: c.registerHandler(),
		},
		&Handler{
			Method:  "GET",
			Path:    "/me",
			Handler: c.getUserHandler(),
		},
	}
}

func (c *UserController) getUserHandler() func(*fiber.Ctx) error {
	return func(fc *fiber.Ctx) error {
		t := fc.Get("Authorization")

		if len(t) <= 0 || !strings.Contains(t, "Bearer ") {
			return fc.JSON(Response{
				Ok:    false,
				Cause: AuthTokenRequired.Error(),
			})
		}

		t = strings.Split(t, " ")[1]
		userInfo, valid := token.VerifyToken(c.cfg.TokenSecret, t)
		if !valid || userInfo == nil {
			return fc.JSON(Response{
				Ok:    false,
				Cause: InvalidAuthToken.Error(),
			})
		}

		user, err := c.userService.GetByGuid(userInfo.ID)
		if err != nil {
			return fc.JSON(Response{
				Ok:    false,
				Cause: err.Error(),
			})
		}

		return fc.JSON(Response{
			Ok:   true,
			User: user,
		})
	}
}

func (c *UserController) registerHandler() func(*fiber.Ctx) error {
	return func(fc *fiber.Ctx) error {
		fc.Accepts("application/json")
		var req RegisterRequest
		if err := fc.BodyParser(&req); err != nil {
			return err
		}

		if errors := RegisterRequest.Validate(req, c.cfg.PasswordMinLength); len(errors) > 0 {
			return fc.JSON(Response{
				Ok:     false,
				Cause:  ValidationErrorError.Error(),
				Errors: errors,
			})
		}

		ctx := context.WithValue(context.Background(), "cfg", c.cfg)
		user, err := c.userService.Register(ctx, &services.NewUser{Login: req.Login, Password: req.Password, Name: req.Name, LastName: req.LastName})
		if err != nil {
			return fc.JSON(Response{
				Ok:    false,
				Cause: err.Error(),
			})
		}

		return fc.JSON(Response{
			Ok:   true,
			User: user,
		})
	}
}
