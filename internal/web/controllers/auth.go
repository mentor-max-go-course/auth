package controllers

import (
	"auth/internal/config"
	"auth/internal/mongodb/models"
	"auth/internal/services"
	"fmt"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

var ValidationErrorError = fmt.Errorf("validation error")

var validate = validator.New()

type ValidationError struct {
	FailedField string
	Tag         string
	Value       string
}

type RefreshAuthRequest struct {
	ID           string `json:"id" validate:"required"`
	RefreshToken string `json:"refresh_token" validate:"required"`
}

func (r RefreshAuthRequest) Validate() []*ValidationError {
	var errors []*ValidationError
	err := validate.Struct(r)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element ValidationError
			element.FailedField = err.StructNamespace()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}
	return errors
}

type AuthRequest struct {
	Login    string `json:"login" validate:"required"`
	Password string `json:"password" validate:"required"`
}

func (r AuthRequest) Validate() []*ValidationError {
	var errors []*ValidationError
	err := validate.Struct(r)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element ValidationError
			element.FailedField = err.StructNamespace()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}
	return errors
}

type AuthResponse struct {
	Ok           bool               `json:"ok"`
	Token        string             `json:"token,omitempty"`
	RefreshToken string             `json:"refresh_token,omitempty"`
	User         *models.User       `json:"user,omitempty"`
	Cause        string             `json:"cause,omitempty"`
	Errors       []*ValidationError `json:"errors,omitempty"`
}

type AuthController struct {
	cfg         *config.AppConfig
	log         *zap.Logger
	authService *services.AuthService
}

func NewAuthController(cfg *config.AppConfig, log *zap.Logger, authService *services.AuthService) *AuthController {
	return &AuthController{
		cfg:         cfg,
		log:         log,
		authService: authService,
	}
}

func (c *AuthController) GetGroup() string {
	return "/auth"
}

func (c *AuthController) GetHandlers() []ControllerHandler {
	return []ControllerHandler{
		&Handler{
			Method:  "POST",
			Path:    "/login",
			Handler: c.authHandler(),
		},
		&Handler{
			Method:  "POST",
			Path:    "/refresh",
			Handler: c.refreshHandler(),
		},
	}
}

func (c *AuthController) authHandler() func(c *fiber.Ctx) error {
	return func(fc *fiber.Ctx) error {
		fc.Accepts("application/json")
		var req AuthRequest
		if err := fc.BodyParser(&req); err != nil {
			return err
		}

		validationErrors := AuthRequest.Validate(req)
		if len(validationErrors) > 0 {
			return fc.JSON(AuthResponse{
				Ok:     false,
				Cause:  ValidationErrorError.Error(),
				Errors: validationErrors,
			})
		}

		authResult := c.authService.Login(req.Login, req.Password)
		if authResult.Err != nil {
			return fc.JSON(AuthResponse{
				Ok:    false,
				Cause: authResult.Err.Error(),
			})
		}

		return fc.JSON(AuthResponse{
			Ok:           true,
			Token:        authResult.Token,
			RefreshToken: authResult.RefreshToken,
			User:         authResult.User,
		})
	}
}

func (c *AuthController) refreshHandler() func(c *fiber.Ctx) error {
	return func(fc *fiber.Ctx) error {
		fc.Accepts("application/json")
		var req RefreshAuthRequest
		if err := fc.BodyParser(&req); err != nil {
			return err
		}

		validationErrors := RefreshAuthRequest.Validate(req)
		if len(validationErrors) > 0 {
			return fc.JSON(AuthResponse{
				Ok:     false,
				Cause:  ValidationErrorError.Error(),
				Errors: validationErrors,
			})
		}

		authResult := c.authService.Refresh(req.ID, req.RefreshToken)
		if authResult.Err != nil {
			return fc.JSON(AuthResponse{
				Ok:    false,
				Cause: authResult.Err.Error(),
			})
		}

		return fc.JSON(AuthResponse{
			Ok:           true,
			Token:        authResult.Token,
			RefreshToken: authResult.RefreshToken,
			User:         authResult.User,
		})
	}
}
