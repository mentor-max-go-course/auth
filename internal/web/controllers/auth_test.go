package controllers

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestAuthRequestValidation(t *testing.T) {
	testCases := []struct {
		Request *AuthRequest
		Valid   bool
		Error   error
	}{
		{
			Request: &AuthRequest{
				Login:    "login",
				Password: "password",
			},
			Valid: true,
		},
		{
			Request: &AuthRequest{
				Login:    "",
				Password: "password",
			},
			Valid: false,
			Error: ValidationErrorError,
		},
		{
			Request: &AuthRequest{
				Login:    "login",
				Password: "",
			},
			Valid: false,
			Error: ValidationErrorError,
		},
	}

	for _, testCase := range testCases {
		errors := testCase.Request.Validate()
		if testCase.Valid {
			require.Empty(t, errors)
		} else {
			require.NotEmpty(t, errors)
		}
	}
}

func TestAuthControllerCreation(t *testing.T) {
	c := NewAuthController(nil, nil, nil)
	require.Equal(t, c, &AuthController{})
}

func TestAuthControllerGroup(t *testing.T) {
	c := NewAuthController(nil, nil, nil)
	require.Equal(t, "/auth", c.GetGroup())
}
