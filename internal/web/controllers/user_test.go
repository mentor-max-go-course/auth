package controllers

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRegisterRequestValidation(t *testing.T) {
	testCases := []struct {
		Request *RegisterRequest
		Valid   bool
		Error   error
	}{
		{
			Request: &RegisterRequest{
				Login:           "login",
				Password:        "password",
				ConfirmPassword: "password",
			},
			Valid: true,
		},
		{
			Request: &RegisterRequest{
				Login:           "",
				Password:        "password",
				ConfirmPassword: "password",
			},
			Valid: false,
			Error: LoginRequiredError,
		},
		{
			Request: &RegisterRequest{
				Login:           "login",
				Password:        "",
				ConfirmPassword: "password",
			},
			Valid: false,
			Error: PasswordRequiredError,
		},
		{
			Request: &RegisterRequest{
				Login:           "login",
				Password:        "password",
				ConfirmPassword: "",
			},
			Valid: false,
			Error: PasswordRequiredError,
		},
		{
			Request: &RegisterRequest{
				Login:           "login",
				Password:        "password",
				ConfirmPassword: "password1",
			},
			Valid: false,
			Error: PasswordNotEqualError,
		},
	}

	for _, testCase := range testCases {
		errors := testCase.Request.Validate(5)
		if testCase.Valid {
			require.Empty(t, errors)
		} else {
			require.NotEmpty(t, errors)
		}
	}
}

func TestUserControllerCreation(t *testing.T) {
	c := NewUserController(nil, nil, nil)
	require.Equal(t, c, &UserController{})
}

func TestUserControllerGroup(t *testing.T) {
	c := NewUserController(nil, nil, nil)
	require.Equal(t, "/auth", c.GetGroup())
}
