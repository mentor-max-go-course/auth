package main

import (
	"auth/internal/config"
	"auth/internal/db"
	"auth/internal/logging"
	"auth/internal/mongodb"
	"auth/internal/services"
	"auth/internal/web"
	"auth/internal/web/controllers"
	"math/rand"
	"os"
	"os/signal"
	"time"

	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	cfg := config.InitConfiguration()
	logger := logging.NewLogger(cfg.Logging, "auth.log")

	logger.Debug("Run with configuration", zap.Any("config", cfg))

	logger.Info("Connecting to MongoDB")
	mongoClient := mongodb.NewMongoDB(logger, cfg.Mongo)
	defer mongoClient.Release()
	err := mongoClient.Connect()
	if err != nil {
		logger.Fatal("Failed connect to MongoDB", zap.Error(err))
	}

	for {
		if mongoClient.IsConnected() {
			logger.Info("Connected to MongoDB")
			break
		}
		time.Sleep(1 * time.Second)
	}

	logger.Info("Connecting to Postgres")
	dbClient, err := db.NewDBConnection(cfg.DB)
	if err != nil {
		logger.Fatal("Failed connect to Postgres", zap.Error(err))
	}

	row := dbClient.QueryRow("select now() as t")
	if row.Err() != nil {
		logger.Error("Failed to get current time", zap.Error(row.Err()))
	}
	var t time.Time
	err = row.Scan(&t)
	if err != nil {
		logger.Error("Failed to get current time", zap.Error(err))
	}
	logger.Info("Current DB Time", zap.String("Time", t.Format("02.01.2006 15:04:05")))

	err = db.ApplyMigrations(cfg.DB.Type, dbClient)
	if err != nil {
		logger.Fatal("Failed to apply migrations", zap.Error(err))
	}

	logger.Info("Migrations applied")

	wApp := web.NewWebServer(logger, cfg.Web)
	registerRoutes(cfg.App, logger, mongoClient, dbClient, wApp)
	go wApp.Run()

	//
	// Graceful Shutdown
	//
	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt, os.Kill)
	takeSig := <-sigChan
	logger.Info("Shutting down gracefully", zap.String("signal", takeSig.String()))
}

func registerRoutes(cfg *config.AppConfig, logger *zap.Logger, mongoClient *mongodb.MongoDB, db *sqlx.DB, wApp *web.WebServer) {
	logger.Info("Registering routes")

	userService := services.NewUserService(logger, mongoClient, db)
	authService := services.NewAuthService(cfg, logger, userService)

	wApp.RegisterRoutes([]controllers.Controller{
		controllers.NewAuthController(cfg, logger, authService),
		controllers.NewUserController(cfg, logger, userService),
	})
}
